/* csplugincommon.h -- Generated automatically; do not edit. */
#ifndef __CSPLUGINCOMMON_H__
#define __CSPLUGINCOMMON_H__
/*
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
  
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
  
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/**@file 
 * Directory master header. This header file includes all headers in a
 * subdirectory of the top Crystal Space include directory.
 */
#include "cssysdef.h"
#include "csplugincommon/canvas/cursorconvert.h"
#include "csplugincommon/canvas/draw_box.h"
#include "csplugincommon/canvas/draw_common.h"
#include "csplugincommon/canvas/draw_line.h"
#include "csplugincommon/canvas/draw_text.h"
#include "csplugincommon/canvas/fontcache.h"
#include "csplugincommon/canvas/graph2d.h"
#include "csplugincommon/canvas/scancode.h"
#include "csplugincommon/canvas/scrshot.h"
#include "csplugincommon/canvas/softfontcache.h"
#include "csplugincommon/canvas/softfontcacheimpl.h"
#include "csplugincommon/imageloader/commonimagefile.h"
#include "csplugincommon/imageloader/optionsparser.h"
#include "csplugincommon/iopengl/driverdb.h"
#include "csplugincommon/iopengl/openglinterface.h"
#include "csplugincommon/particlesys/partgen.h"
#include "csplugincommon/render3d/normalizationcube.h"
#include "csplugincommon/render3d/txtmgr.h"
#include "csplugincommon/renderstep/basesteploader.h"
#include "csplugincommon/renderstep/basesteptype.h"
#include "csplugincommon/renderstep/parserenderstep.h"
#include "csplugincommon/script/scriptcommon.h"
#include "csplugincommon/shader/shaderplugin.h"
#include "csplugincommon/shader/shaderprogram.h"
#include "csplugincommon/shader/weavercombiner.h"
#include "csplugincommon/shader/weavertypes.h"
#include "csplugincommon/sndsys/convert.h"
#include "csplugincommon/sndsys/cyclicbuf.h"
#include "csplugincommon/sndsys/queue.h"
#include "csplugincommon/sndsys/snddata.h"
#include "csplugincommon/sndsys/sndstream.h"
#include "csplugincommon/softshader/defaultshader.h"
#include "csplugincommon/softshader/renderinterface.h"
#include "csplugincommon/softshader/scanline.h"
#include "csplugincommon/softshader/texture.h"
#include "csplugincommon/softshader/types.h"
#endif /* __CSPLUGINCOMMON_H__ */


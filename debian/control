Source: crystalspace
Section: devel
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Christian Bayle <bayle@debian.org>, Barry deFreese
 <bdefreese@debian.org>, Devid Antonio Filoni <d.filoni@ubuntu.com>
Build-Depends: python-support (>= 0.4), debhelper(>= 5), nasm (>= 0.98.08),
 lib3ds-dev (>= 1.2.0), libogg-dev (>= 1.0rc2), libmikmod2-dev,
 libvorbis-dev (>>1.0.0), docbook-to-man, libgl1-mesa-dev, zip,
 libpng3-dev, libjpeg62-dev, libfreetype6-dev, zlib1g-dev, libode-dev,
 libopenal-dev, libalut-dev, libcal3d-dev, swig, dh-buildinfo, flex,
 bison, texinfo, doxygen, gs-common, freeglut3-dev, libmng-dev, libsdl1.2-dev,
 autoconf, libx11-dev, libxext-dev, libxxf86vm-dev, libcaca-dev,
 libwxgtk2.8-dev, libcegui-mk2-dev, jam, quilt
Standards-Version: 3.8.3
Homepage: http://www.crystalspace3d.org
Vcs-Git: git://git.debian.org/git/pkg-games/crystalspace.git

Package: crystalspace
Architecture: any
Depends: ${shlibs:Depends}, ${python:Depends}, ${misc:Depends}
Suggests: crystalspace-doc, crystalspace-dev
Description: Multiplatform 3D Game Development Kit
 Crystal Space is a free 3D game toolkit. It can be used for a variety
 of 3D visualization tasks. Many people will probably be interested in using
 Crystal Space as the basis of a 3D game, for which it is well suited. 
 It is divided in a main package containing engine, a dev package
 containing dev related utilities, a doc package containing extended html
 documentation.

Package: crystalspace-dev
Architecture: any
Depends: crystalspace (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: Multiplatform 3D Game Development Kit dev files
 Crystal Space is a free 3D game toolkit. It can be used for a variety
 of 3D visualization tasks. Many people will probably be interested in using
 Crystal Space as the basis of a 3D game, for which it is well suited. 
 This dev part contains dev utilities, like tools to create levels, and
 a config utility.

Package: crystalspace-doc
Section: doc
Depends: ${misc:Depends}
Architecture: all
Description: Multiplatform 3D Game Development Kit Documentation
 Crystal Space is a free 3D game toolkit. It can be used for a variety
 of 3D visualization tasks. Many people will probably be interested in using
 Crystal Space as the basis of a 3D game, for which it is well suited. 
 This doc part contains an extended html documentation, describing the engine, 
 API documentation, and a dev tutorial.

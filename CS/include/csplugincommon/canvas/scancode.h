/*
  Scan code support for Crystal Space 3D library
  Copyright (C) 1998 by Jorrit Tyberghein
  Written by Andrew Zabolotny <bit@eltech.ru>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __CS_CSPLUGINCOMMON_CANVAS_SCANCODE_H__
#define __CS_CSPLUGINCOMMON_CANVAS_SCANCODE_H__

/**\file
 * Scan code to character conversion table.
 */

/**
 * \addtogroup plugincommon
 * @{ */

/// This array can be used to translate scancodes into Crystal Space codes
extern CS_CRYSTALSPACE_EXPORT const unsigned short ScanCodeToChar [128];

/** @} */

#endif // __CS_CSPLUGINCOMMON_CANVAS_SCANCODE_H__

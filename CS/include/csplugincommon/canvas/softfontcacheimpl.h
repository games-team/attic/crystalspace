/*
    Copyright (C) 2004 by Jorrit Tyberghein
	      (C) 2004 by Frank Richter

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __CS_CSPLUGINCOMMON_CANVAS_SOFTFONTCACHEIMPL_H__
#define __CS_CSPLUGINCOMMON_CANVAS_SOFTFONTCACHEIMPL_H__

/**\file
 * Actual implementation of a font cache for software canvases.
 */

#include "graph2d.h"
#include "csplugincommon/canvas/draw_text.h"

/**
 * \addtogroup plugincommon
 * @{ */

/**
 * An actual implementation of a font cache for software canvases.
 * Templated to allow flexible support for different pixel formats.
 * \a Tpixel defines how a pixel is stored in video memory (usually
 * uint16 or uint32), \a Tpixmixer defines the color encoding.
 */
template <class Tpixel, class Tpixmixer>
class csSoftFontCacheImpl : public csSoftFontCache
{
  void WriteString (iFont *font, int x, int y, int fg, int bg, 
    const void* text, bool isWide, uint flags)
  {
    x += vpX; y += vpY;
    
    int realColorFG;
    uint8 alphaFG;
    SplitAlpha (fg, realColorFG, alphaFG);
    int realColorBG;
    uint8 alphaBG;
    SplitAlpha (bg, realColorBG, alphaBG);

    if (alphaBG == 0)
    {
      if (alphaFG == 0)
	return;
      realColorBG = realColorFG;
      if (alphaFG == 255)
      {
	csG2DDrawText<Tpixel, csPixMixerCopy<Tpixel>, csPixMixerNoop<Tpixel>, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
      else
      {
	csG2DDrawText<Tpixel, Tpixmixer, csPixMixerNoop<Tpixel>, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
    }
    else if (alphaBG == 255)
    {
      if (alphaFG == 0)
      {
	csG2DDrawText<Tpixel, csPixMixerNoop<Tpixel>, csPixMixerCopy<Tpixel>, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
      else if (alphaFG == 255)
      {
	csG2DDrawText<Tpixel, csPixMixerCopy<Tpixel>, csPixMixerCopy<Tpixel>, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
      else
      {
	csG2DDrawText<Tpixel, Tpixmixer, csPixMixerCopy<Tpixel>, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
    }
    else
    {
      if (alphaFG == 0)
      {
	csG2DDrawText<Tpixel, csPixMixerNoop<Tpixel>, Tpixmixer, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
      else if (alphaFG == 255)
      {
	csG2DDrawText<Tpixel, csPixMixerCopy<Tpixel>, Tpixmixer, 
	  Tpixmixer>::DrawText (this, font, x, y, realColorFG, alphaFG, 
	  realColorBG, alphaBG, text, isWide, flags);
      }
      else
      {
	csG2DDrawText<Tpixel, Tpixmixer, Tpixmixer, Tpixmixer>::DrawText (
	  this, font, x, y, realColorFG, alphaFG, realColorBG, alphaBG, text, 
	  isWide, flags);
      }
    }
  }
public:
  csSoftFontCacheImpl (csGraphics2D* G2D) : csSoftFontCache (G2D)
  {
  }
  virtual void WriteString (iFont *font, int x, int y, int fg, int bg, 
    const utf8_char* text, uint flags)
  {
    if (!text || !*text) return;
    
    WriteString (font, x, y, fg, bg, text, false, flags);
  }
  virtual void WriteString (iFont *font, int x, int y, int fg, int bg, 
    const wchar_t* text, uint flags)
  {
    if (!text || !*text) return;
    
    WriteString (font, x, y, fg, bg, text, true, flags);
  }
};

/** @} */

#endif // __CS_CSPLUGINCOMMON_CANVAS_SOFTFONTCACHEIMPL_H__
